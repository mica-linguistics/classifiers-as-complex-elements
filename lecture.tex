\input lecturemac

\vbox{\offinterlineskip
 \hbox{\titlefont\spaceskip0.25em Classifiers as Complex Elements}\vskip6pt
 \hbox{\subtitlefont\hskip1pt Mica Clausen\decoration October 5,\ 2023\decoration NYU:\ Grammatical Diversity}}

\bigskip

Almost all analyses of classifiers have taken them to be heads of a unique functional projection within the~DP (e.g.\ Cheng~and Sybesma 2005:\null~280; Simpson 2005:\null~807--810; Jenks 2011:\null~201, ``Classifiers as functional heads'').

But: Are classifiers heads? Are they a unique syntactic category? Is what we call a classifier even a single syntactic category?

I will argue that classifiers are syntactically complex, consisting of at least: (*a*)~a phrasal element (possibly an~NP), and at least in some languages, (*b*) a silent definite article.

\begingroup
\sectionnosfalse

\subsection Roadmap

\parindent2.5em
\parskip1pt

\item{\sans1\enspace} Against classifiers as heads
\itemitem{\sans1.\rlap1\quad} Compound repeaters in Thai
\itemitem{\sans1.\rlap2\quad} Nominalizations in Ch'ol
\item{\sans2\enspace} Definiteness effects in Thai and elsewhere
\itemitem{\sans2.\rlap1\quad} Classifier spreading
\itemitem{\sans2.\rlap2\quad} Demonstratives
\item{\sans3\enspace} Conclusions

\endgroup




\section Against classifiers as heads

\subsection Compound repeaters in Thai

Numerically quantified~DPs in Thai have the order N Num CLF:

\beginex
\(\nnumcl) \{thurijn\} \{sam\} \{lUk\}\tfil(*Thai*,~Jenks~2011:\null~77)\cr\noalign{\vskip2pt}%
           t\h\'urian  s\>aam  l\<uuk
           durian      three  @CLF@.ball
           |``three durians''|
\endex

Hundreds (possibly an unbounded number) of words can be repeated in the position for nouns and classifiers simultaneously:

\beginex
\(\repeater) \{khn\} \{sam\} \{khn\}\tfil(*Thai*,~Hundius~and~K\:olver~1983:\null~190)
             k\h{}on s\>aam  k\h{}on
             person  three   person
             |``three people''|
\endex


Interestingly, these ``repeaters'' are allowed to be compounds:

\beginex
\tfil(*Thai*,~Hundius~and~K\:olver~1983:\null~184)\cr\noalign{\medskip}%
\(\lakes) \{th?el\} \{sab\}   ~ \{sam\} ~ \{th?el\} \{sab\}
          t\h{}alee \-s\`aap  ~ s\>aam  ~ t\h{}alee \-s\`aap
          sea-freshwater\span ~ three   ~ sea-freshwater\span
          |``three lakes''|
\endex

\beginex
\noalign{\bigbackskip}\tfil(D\'ek\'any~in~prep:\null~10,~fn.\,8)\cr\noalign{\medskip}%
\(\superpowers) \{pr?ethS\} \{mha\}  \{OMnac\}  ~ \{sam\} ~ \{pr?ethS\} \{mha\}  \{OMnac\}
                prat\h\<eet \-mah\>a \-amn\<aat ~ s\>aam  ~ prat\h\<eet \-mah\>a \-amn\<aat
                country-great-power\span\span   ~ three   ~ country-great-power\span\span
                |``three superpowers''|
\endex

Supposing that these compounds are phrasal (cf.\ Koopman~and Szabolcsi 2000; Koopman 2005; Collins~and Kayne 2023:\null~9, fn.\,2), and adopting the analysis that repeaters are nouns that are syntactically copied into ``classifier position'' (Simpson~2005:\null~832--833), then the above indicate that ``classifier position'' cannot be a head.

\subsection Nominalizations in Ch'ol

Bale et al.\ (2019) note that classifiers in Ch'ol are often ``derived from'' verbs by adding an orthographic~\=j- (=~phonological~[h]).

\beginex
\tfil(*Ch'ol*,~Bale~et~al.\null~2019:\null~14,\ 12)\cr\noalign{\bigskip}%
\(\deer) a.~ **Koty**-ol         ji\~ni me'.
         ~   on.all.fours-@STAT@ @DET@  deer
         ~   |``The deer is standing on four legs.''|
\endex
\beginex
\?{\(\deer)} b.~ ch\:a\~n-**kojty** me'
             ~   four-@CLF@         deer
             ~   |``four deer''|
\endex
\beginex
\(\carrywood) a.~ Ta'  i-**kuch**-u   si'  ji\~ni wi\~nik.
              ~   @PFV@ he-carry-@TV@ wood @DET@  man
              ~   |``The man carried firewood.''|
\endex
\beginex
\?{\(\carrywood)} b.~ cha'-**kujch**  si'
                  ~   two-@CLF@ wood
                  ~   |``two loads of firewood''|
\endex

Bale et al.\ take this to be a diachronic process. Is there a synchronic explanation?

Interestingly, passives in Ch'ol are also formed via~\=j-.

\beginex
\(\woodcarried) Ta'    **kujch**-i         si'.\tfil(*Ch'ol*,~Bale~et~al.\null~2019:\null~12)
                @PFV@  carry.@PASS@-@ITV@  wood
                |``Firewood was carried.''|
\endex

Why would passives and classifiers take the same morpheme?

Law (2020:\null~368) notes that \=j\= was a nominalizer in Proto-Mayan, and Law~and Stuart (2017:\null~156--157) give some examples of these nominalizations from Classic (i.e.\ written) Mayan, where \=j\= was realized as a long vowel.

\beginex
\(\mayan) a.~muk\?u~`bury'\tfil{}b.~pas\?a~`open'   c.~tzutz\?u~`finish'
          \?{a.~}muuk~`tomb'\tfil\?{b.~}paas~`door' \?{c.}~tzuutz~`end'
\endex

If -j- is a nominalizer in Ch'ol, then maybe both its passives and classifiers are nominalizations.

\beginex
\(\carryingwood) a.~ Ta'   kujch-i            si'.\tfil(*Ch'ol*)
                 ~   @PFV@ **carrying**-@ITV@ wood
                 ~   |``Carrying of firewood happened.''|
\endex
\beginex
\?{\(\carryingwood)} b.~ cha'-kujch       si'
                     ~   two-**carrying** wood
                     ~   |``two carryings of firewood''|
\endex

Bale et al.\ (2019:\null~25) themselves even note that \hbox{*lujch*} (`to~ladle' + \=j\=) might as well be glossed as ``ladlings.'' And this isn't the first time someone has suggested that some Ch'ol clauses look like nominalizations (cf.\ Coon~2010).

If this analysis is on the right track, then what's called a classifier in Ch'ol is morphologically complex---at least a verb and a nominalizing element---and therefore (once again assuming that compounds are phrases) is not likely to be a head.\fn1 The data above point towards their being nominal in nature, possibly just plain~NPs.

An interesting question, if classifiers are indeed phrasal, is what category they are merged with.


\section Definiteness effects in Thai and elsewhere

Both Simpson (2005) and Cheng~and Sybesma (2005) tie classifiers to definiteness. We can see definiteness effects from classifiers in two interesting ways in Thai.


\subsection Classifier spreading

Modifiers (adjectives, relative clauses, etc.)\ are postnominal in Thai. These Ns are ambiguous for (in)\hglue\nil definiteness and plurality.

\beginex
\(\stinkydurian) \{thurijn\} \{thI`\}  \{ehm;n\}\tfil(*Thai*,~Jenks~2011:\null~203)\cr\noalign{\vskip2pt}%
                 t\h\'urian  t\h\<ii   m\>en
                 durian      @REL@     stinky
                 |``a/the durian(s) that is/are stinky''|
\endex

Nouns can be linked to their modifier by a classifier. This makes the nominal obligatorily definite.\fn{2}

\beginex
\(\thestinkydurian) \{thurijn\} \{lUk\}    \{thI`\} \{ehm;n\}\tfil(Jenks~2011:\null~203)\cr\noalign{\vskip2pt}%
                    t\h\'urian  l\<uuk     t\h\<ii  m\>en
                    durian      @CLF@.ball @REL@    stinky
                    |``the durian that is stinky''|
\endex

This construction can be iterated, with multiple identical copies of the classifier appearing before each modifier.

\beginex
\tfil(*Thai*,~Hundius~and~K\:olver~1983:~177)\cr\noalign{\bigskip}%
\(\cs) \{r`m\}  \{khAn\}  \{sI\} \{ekHIjw\} \{khAn\}  \{YhJ`\}
       r\<om    **k\hh{}an**   s\>ii  \-k\h\>iaw **k\hh{}an**   j\`aj
       umbrella @CLF@.rod color-green\span  @CLF@.rod big
       |``the big green umbrella''|
\endex

Jenks (2006) observed that this is reminiscent of a construction in Greek known as ``determiner spreading.''

\beginex
\tfil(*Greek*,~Alexiadou~and~Wilder~1998:~303)\cr\noalign{\bigskip}%
\(\ds) \^to\^  \^bibl\\"D0o\^  \^to\^  \^k\\"ECkkino\^  \^to\^  \^meg\\"88lo\^
       **to**  vivlio          **to**  kokkino          **to**  megalo
       the     book            the     red              the     big
       |``the big red book''|
\endex

The surface distribution of the classifiers in~\(\cs) is remarkably similar to the definite articles in~\(\ds). Each modifier is preceded by a classifier or definite article. (We might want to say that~\(\cs) is an example of ``classifier spreading.'')

These constructions also share a number of syntactic and interpretive properties. First, modifiers not allowed as predicates in root clauses also aren't allowed in determiner\slash classifier spreading constructions.

\beginex
\noalign{\bigbackskip}\tfil(*Greek*,~Alexiadou~and~Wilder~1998:\null~306)\cr\noalign{\bigskip}%
\(\murderer) a.~ \* \^o\^  \^upotij\\"E8menoc\^  \^o\^  \^dolof\\"ECnoc\^
             ~   ~  o      ipotithemenos            o      dolofonos
             ~   ~  the    alleged                  the    murderer
\endex
\beginex
\?{\(\murderer)} b.~ \* \^o\^  \^dolof\\"ECnoc\^  \^\\"A0tan\^ \^upotij\\"E8menoc\^
                 ~   ~  o      dolofonos             itan             ipotithemenos
                 ~   ~  the    murderer              was              alleged
                 ~   ~  |``The murderer was alleged.''|
\endex

\beginex
\tfil(*Thai*,~Kookiattikoon~2001:\,189)\cr\noalign{\bigskip}%
\(\teacher) \* \{khrU\} \{khn\}      \{fisiks\*\}\cr\noalign{\vskip1pt}%
             ~  k\h{}ru k\h{}on      f\'is\`ik
             ~  teacher @CLF@.person physics
             ~  |Intended: ``the physics teacher''|
\endex

This suggests that the modifiers in determiner\slash classifier spreading constructions are in a predicate position, i.e.\ ``the murderer who was alleged,'' ``the teacher who was physics.''

Second: in addition to the adjective being a predicate, it must be restrictive:

\beginex
\(\researchers) \^oi\^ \^mikr\\"E8c\^  \^oi\^  \^g\\"88tec\^ \tfil (*Greek*,~Kolliakou~2004:\null~271)
                i      mikres          i       gates
                the    young           the     cats
                |\entails ``the young ones among the cats''|
                |\nentails ``the cats, all of which are young''|
\endex
\beginex
\tfil(*Thai*,~Catford~et~al.\null~1974:\null~50)\cr\noalign{\bigskip}%
\(\friend) \{ephV`On\} \{pHm\} \{khn\}   \{thI`\} \{OjU`\} \{thI`\} \{orngrIjn\}
           p\h\<i\#{}an p\h\>om k\h{}on   t\h\<ii  j\`uu    t\h\<ii  ro\ng{}rian
           friend   my      @CLF@.person @REL@    live     at       school
           |\entails ``the (only) one of my friends who lives at school''|
           |\nentails ``my friend, who lives at school''|
\endex

Based on these interpretive properties, Alexiadou~and Wilder (1998) present an analysis of Greek determiner spreading as being multiply nested (restrictive) relative clauses.

They adopt the idea from Kayne (1994) that relative clauses have a structure [D~CP]. The reason for multiple definite articles, they argue, is that each new clausal layer is introduced by a new~D. The derivation for a single predicate is roughly:

\beginex
\(\dsstruc) a.~ ~     ~   ~     ~   [[@DP@ the book] red]\tfil(pred~struc.)
            b.~ ~     ~   [@CP@ red [[@DP@ the book] \hfill{t}]]\tfil(pred~mvt.)
            c.~ [@DP@ the [@CP@ red [[@DP@ the book] \hfill{t}]]\rlap]\tfil(merge~*the*)
\endex

Jenks (2011) concludes the same for Thai, but argues that the classifier spreading construction has a silent definite article:\,\fn{3}

\beginex
\(\csstrucjenks) D [[umbrella @CLF@] green] \tfil(cf.\null~Jenks~2011:\null~224)
\endex

But recall that the classifiers in Thai are distributed like definite articles in Greek. We should therefore conclude that classifiers are in a very local relationship with~D. Suppose a derivation starting from~\(\csstrucjenks), which has the classifier moving to~SpecDP and the nominal remnant moving above that:

\beginex
\(\csstruc) % a. ~         ~  ~     D [[umbrella @CLF@] green] \tfil(pred~struc.)
            a. ~         ~  @CLF@ D [[umbrella t]     green] \tfil(clf~mvt.)
            b. [umbrella t] @CLF@ D [t         ~      green] \tfil(nom~mvt.)
\endex

When repeated, this gives the (simplified) structure:

\beginex
\(\csstrucrepeat) [[[umbrella] @CLF@ D green] @CLF@ D big]
\endex



Finally, what enforces identicalness of definiteness features among Ds in Greek (i.e.\ why are they all *definite* and not *in\kern0.5pt*definite)? Alexiadou~and Wilder (1998) argue that it's done via an agreement mechanism.

Recall that classifiers in Thai must also be identical. But agreement in classifiers would not work. Simply put, there are too many of them in Thai. Agreement would have to be in a single feature with hundreds (or an unbounded number) of values. No such agreement mechanism has ever been proposed, and it would stretch the limits of what we think syntactic agreement can do.

In order to enforce the identicalness of classifiers in classifier spreading, something like syntactic copying is needed instead (cf.\ Jenks 2011:\null~129, fn.\,24).\fn{4}






\subsection Demonstratives

Let's look at another construction in Thai which is obligatorily definite and which also involves classifiers: demonstrative constructions.

\beginex
\(\thisclbook) \{hnAngsV\}      ~ \{el`m\}     ~ \{nI<\}\tfil(*Thai*,~Jenks~2011:\null~161)
               n\>a\ng-si\#i\# ~ l\<em        ~ n\'ii
               book         ~ @CLF@.volume ~ @PROX@
               |``this book''|
\endex

Many analyses have tied the definiteness of~\(\thisclbook) to the presence of the element *n\'ii* (cf.\ e.g.\ Simpson 2005:\ 824--825; Jenks 2011:\null~132).



But let me suggest that it is once again the classifier which is introducing definiteness, in a way that somewhat parallels the definiteness of classifier spreading constructions---but unlike those constructions, does not involve a relative clause.

First, note that the classifier in~\(\thisclbook) can be omitted:

\beginex
\(\thisbook) \{hnAngsV\}       ~ \{nI<\}\tfil(*Thai*,~Visonyanggoon~2000:\null~100)
             n\>a\ng-si\#i\# ~ n\'ii
             book              ~ @PROX@
             |``this book''|
\endex

What's the difference in meaning between~\(\thisclbook) and~\(\thisbook)?

Maclaran (1980) and Bernstein (1997) observe that demonstratives in Germanic and Romance have both a definite reading and an indefinite reading.

\beginex
\(\defdem) a. |With **this** ring, I thee wed.|\tfil(definite)\?{in}
           b. |Quick, hand me **that** book!|
\endex
\beginex
\(\indefdem) a. |So there's **this** guy I've been seeing.|\tfil(indefinite)
             b. |He's a piece of work, **that** guy.|
\endex

Given the connection we saw in the last section between classifiers and definite articles, we ought to wonder whether~\(\thisclbook) is definite and~\(\thisbook) is indefinite. How can we test for this?

Bernstein shows that the definite reading of demonstratives is incompatible with restrictive relatives. In French, a demonstrative with *ici* (`here') can only be definite (Bernstein~1997:\null~97), and such demonstratives aren't allowed in a restrictive relative.

\beginex
\tfil(*French*,~Bernstein~1997:\null~102)\cr\noalign{\medskip}%
\(\french) ce  livre-(\*ci) que  j'ai   achet\'e
           the book-here    that I.have bought
           |``this book that I bought''|
\endex

I think we see the same facts in Thai. In restrictive relatives (recall from the last section that a modifier linked by a classifier is restrictive), demonstratives without classifiers are allowed~\(\thatredshirt a), but demonstratives with classifiers aren't~\(\thatredshirt b).

{\numskip\glossskip\beginex
\(\thatredshirt) \tfil(*Thai*,~Chaiphet~2021:\null~20;\null~37,\null~fn.\,9)\cr\noalign{\bigskip}%
\?(a. \{esV<O\}  \?[\{tAw\} \{sIEdng\}           \{hnv`ng\}     \?[\{tAw\}  \{thI`\} \{cHAn\} \{zV<O\} \{ma\} \{nA<n\}
   s\<i\#{}a     [tua        s\>ii-d\E\E\ng] n\`i\#\ng     [tua      t\h\<ii  tc\h\'an s\'i\#i\#  maa]   **n\'an**
   shirt      \?[@CLF@ color-red              one            \?[@CLF@    @REL@    I        buy      come   @MED@
   |``that one red shirt I bought''|
\endex
\beginex
\rlap{\?(b.}\?{\(\thatredshirt)}%
   \{esV<O\} \?[\{tAw\} \{sIEdng\}         \?[\{tAw\}  \{thI`\} \{cHAn\} \{zV<O\} \{ma\} \?{\*(}\{tAw\} \{nA<n\}
   s\<i\#{}a    [tua     s\>ii-d\E\E\ng]    [tua      t\h\<ii  tc\h\'an s\'i\#i\#  maa]      \*(**tua**  **n\'an**)
   shirt     \?[@CLF@   color-red          \?[@CLF@    @REL@    I        buy      come   \?{\*(}@CLF@   @MED@
   |Intended: ``that red shirt I bought''|
\endex}

And Visonyanggoon (2000) indicates that when you have an adjective a demonstrative each with a classifier, a prosodic break is required in between.

\beginex
\noalign{\medbackskip}\tfil (*Thai*,~Visonyanggoon~2000:\null~105,~fn.\,4)\cr\noalign{\bigskip}%
\(\tallwoman) \{pHU<h\\"ADing\} \{khn\}      \{sUng\} ~    \{khn\}      \{nA<n\}\cr\noalign{\vskip2pt}%
              p\h\<uu-j\>i\ng    k\h{}on      s\>uu\ng ~//~ k\h{}on      n\'an
              woman             @CLF@.person tall     ~    @CLF@.person @MED@
              |``that woman, the tall one''|
\endex

This is exactly what we'd expect if (*a*)~classifier~+ adjective is a restrictive relative, (*b*)~classifier~+ *n\'an* is a definite demonstrative, and (*c*)~if definite demonstratives aren't compatible with restrictive relatives.\fn{5}

The data are preliminary, but they suggest that definiteness can't be tied to the elements *n\'ii*\slash*n\'an* as others have suggested. In particular, Simpson's (2005) suggested derivation of~\(\thisclbook), where it would be *n\'ii*~(`@PROX@') in SpecDP, with the rest of the nominal raised above it, would not work.

\beginex
\(\simpsonderiv) [book @CLF@] [n\'ii D \dots] \tfil(*Thai*,~cf.\null~Simpson~2005:\null~828)
\endex

Instead, definiteness should be tied to the presence of the classifier. Moreover, the similarity in distribution between classifiers and definite articles (in e.g.\ classifier spreading constructions) tells us that the classifier should be local to~D.

Suppose, instead of~\(\simpsonderiv), that either the classifier is in SpecDP and *n\'ii* is left in some lower position in the nominal~\(\myderiv a), or @CLF@~and *n\'ii* together form a constituent in~SpecDP \(\myderiv b).

\beginex
\(\myderiv) a.~ |book [@CLF@ D n\'ii]|\tfil(*Thai*)
            b.~ |book [@CLF@ n\'ii] D|
\endex

Like the proposed derivation for classifier spreading in~\(\csstrucrepeat), both of these have the classifier in a local relationship with~D. The structures in~\(\myderiv) also make different (and I think more plausible) predictions for the constituency of demonstratives than~\(\simpsonderiv).



\section Conclusions

We saw two ways that classifiers induce definiteness effects in Thai, ``classifier spreading'' and demonstratives.

But classifiers are not themselves definite articles (contra Cheng and Sybesma 2005). Instead, they look more like N(P)s (e.g.\ Thai compound repeaters, Ch'ol nominalizations).

A possibility is that classifiers are NPs in the Spec of a silent definite~D.\fn{6}

This still leaves many questions open.

\begingroup
\medskip
\parindent1.5em
\parskip4pt
\item{$\bullet$} Does a silent D accompany classifiers in all languages?
\item{$\bullet$} What role does (the lack of) the plural play?
\item{$\bullet$} What's the connection between definite articles and plurals?
\item{$\bullet$} What about numerals?
\item{$\bullet$} (How) does the definite article play a role in the semantics of *individuation* (Krifka~1995, Chierchia~1998)?
\medskip
\endgroup





\input ref
\input lingrefs
\sectionnosfalse

\section References

\vskip-2pt
\parindent\nil
\justified
\parskip3pt
\everypar{\hangafter1\hangindent2em}
\hyphenpenalty5000
\tolerance1000
\frenchspacing

\getref{Alexiadou and Wilder 1998}
\getref{Bale et al 2019}
\getref{Bernstein 1997}
\getref{Catford et al 1974}
\getref{Chaiphet 2021}
\getref{Cheng and Sybesma 2005}
\getref{Chierchia 1998}
\getref{Collins and Kayne 2023}
\getref{Coon 2010}
\getref{D\'ek\'any in prep}
\getref{Hundius and K\:olver 1983}
\getref{Jenks 2006}
\getref{Jenks 2011}
\getref{Kayne 1994}
\getref{Kayne 2008}
\getref{Kolliakou 2004}
\getref{Koopman 2005}
\getref{Koopman and Szabolcsi 2000}
\getref{Kookiattikoon 2001}
\getref{Krifka 1995}
\getref{Law 2020}
\getref{Law and Stuart 2017}
\getref{Maclaran 1980}
\getref{Pichetpan and Post 2021}
\getref{Simpson 2005}
\getref{Visonyanggoon 2000}