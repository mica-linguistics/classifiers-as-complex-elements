# Description

This was a talk I gave at NYU in October of 2023 called 'Classifiers as Complex Elements' in Gary Thoms and Richie Kayne's *Grammatical Diversity* course.

I wanted to try to account for the fact that classifiers look morphologically like NPs (in particular, complex NPs), but also give rise to definiteness interpretations. I speculated that classifiers are NPs in the Spec of a silent \[+definite\] D. It remains to be seen whether this can work for all cases (esp. for numeral-classifier-noun constructions, though cf. Cheng and Sybesma's 2005: 284 idea that "'a book' in Chinese languages is literally “\[one \[the book\]\].'"), and whether this analysis is compatible with one where classifiers are in complementary distribution with plural morphemes (the "Greenberg-Sanches-Slobin generalization").

# Files

This repository contains a PDF and three plain TeX files:

- `lecture.pdf` (pdf)
- `lecture.tex` (main TeX file)
- `lecturemac.tex` (macros)
- `lecturenotes.tex` (footnotes)

The files above reference some custom macro files I keep in my local repository (`lingtools.tex`, `parts.tex`, `typography.tex`, `multicolumn.tex`, `ref.tex`, `lingrefs.tex`). I didn't include them here, but [email me](mailto:mdc.clausen@gmail.com) if you want to see those. I may upload them here someday.

# Fonts

The main body font is [MLModern](https://ctan.org/pkg/mlmodern), the Thai font is [Garuda](https://ctan.org/tex-archive/language/chinese/CJK/cjk-4.8.5/utils/thaifont/texmf/fonts), and the Greek font is [CBGreek](https://ctan.org/pkg/cbgreek-complete). The Thai font uses a custom virtual font so that it works with ASCII inputs.
